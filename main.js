/*
1. Display Maze
    - convert map array into html elements
        A div for each row & a div for each block inside that row
    - insert those elements into the dom
    - Style the elements using css
2. Allow player to move through maze
3. Win Condition
*/


const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let playerElement = document.createElement("div")
playerElement.id = "player"
playerElement.style.height = "45px"
playerElement.style.width = "45px"
playerElement.style.backgroundImage = "url(spongebob.png)"
// Image = url(spongebob.png)


const displayMazeElement = (function (rowString, rowIndex) {
    //create & display row div
    let rowDiv = document.createElement("div")
    rowDiv.classList.add("row")
    // maze.appendChild(rowDiv)

    for (let colIndex = 0; colIndex < rowString.length; colIndex++) {
        let block = rowString[colIndex]
        const div = document.createElement("div")
        div.dataset.column = colIndex
        div.dataset.row = rowIndex

        document.getElementById("maze").appendChild(div)
        div.style.width = '45px'
        div.style.height = '45px'
        div.style.display = "inline-block"
        

        document.getElementById("maze").appendChild(div)

        if (block === "W") {
            div.classList.add("wall")
        } else if (block === " ") {
            div.classList.add("space")
        } else if (block === "S") {
            div.classList.add("start")
            div.appendChild(playerElement)
        } else if (block === "F") {
            div.classList.add("finished")
        }
        rowDiv.appendChild(div)
    }
    document.getElementById("maze").appendChild(rowDiv)
})

map.forEach(displayMazeElement)


//Moving SpongeBob
// I got help from Dean

function winner(){
    let popUp = document.createElement(div)
    popUp.textContent = "YOU WIN"
    div.style.height = "100px"
    div.style.width = "100px"
    document.body.appendChild(div)
}

document.addEventListener("keydown", movePlayer)

function movePlayer(event) {
    //console.log(document.querySelector("[data-column='1'][data-row='2'"))
    let playerElement = document.getElementById("player")
    let playerPositionColumn = Number(playerElement.parentElement.dataset.column)
    let playerPositionRow = Number(playerElement.parentElement.dataset.row)
    if (event.key === "ArrowDown") {
        let downElement = (document.querySelector(`[data-column='${playerPositionColumn}'][data-row='${playerPositionRow + 1}'`))
        if (downElement.className !== "wall") {
            downElement.appendChild(playerElement)
        }
    } else if (event.key === "ArrowUp") {
        let upElement = (document.querySelector(`[data-column='${playerPositionColumn}'][data-row='${playerPositionRow - 1}'`))
        if (upElement.className !== "wall") {
            upElement.appendChild(playerElement)
        }
    } else if (event.key === "ArrowLeft") {
        let leftElement = (document.querySelector(`[data-column='${playerPositionColumn - 1}'][data-row='${playerPositionRow}'`))
        if (leftElement.className !== "wall") {
            leftElement.appendChild(playerElement)
        }
    } else if (event.key === "ArrowRight") {
        let rightElement = (document.querySelector(`[data-column='${playerPositionColumn + 1}'][data-row='${playerPositionRow}'`))
        if (rightElement.className !== "wall") {
            rightElement.appendChild(playerElement)
        }
    } 

}





